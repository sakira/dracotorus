﻿#pragma once

#include <cstdint>
#include <vector>

class Torus
{
public:
	Torus(float radiusTorus, float radiusTube);
	Torus() = delete;
	~Torus() = default;

	void Generate(size_t countTorus, size_t countTube);
	const std::vector<float>& GetPositions() const;
	const std::vector<float>& GetNormals() const;
	const std::vector<uint32_t>& GetIndices() const;

private:
	const float m_radiusTorus;
	const float m_radiusTube;
	std::vector<float> m_positions;
	std::vector<float> m_normals;
	std::vector<uint32_t> m_indices;
};

