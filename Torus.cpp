﻿#include "Torus.h"
#include <iostream>

Torus::Torus(float radiusTorus, float radiusTube) :
	m_radiusTorus{radiusTorus},
	m_radiusTube{radiusTube}
{
	if (radiusTorus <= radiusTube)
		std::cout << "トーラスの形が不正です。" << std::endl;
}

void Torus::Generate(size_t countTorus, size_t countTube)
{
	static const float TwoPi = atanf(1.f) * 8.f;

	m_positions.resize(countTorus * countTube * 3);
	m_normals.resize(countTorus * countTube * 3);
	m_indices.resize(countTorus * countTube * 6);

	for (size_t indexTorus = 0; indexTorus < countTorus; ++indexTorus)
	{
		const float angleTorus = indexTorus * TwoPi / countTorus;
		for (size_t indexTube = 0; indexTube < countTube; ++indexTube)
		{
			const float angleTube = indexTube * TwoPi / countTube;
			const size_t index = indexTorus * countTube + indexTube;

			// 位置座標
			m_positions[index * 3 + 0] = (m_radiusTorus + m_radiusTube * cosf(angleTube)) * cosf(angleTorus);
			m_positions[index * 3 + 1] = (m_radiusTorus + m_radiusTube * cosf(angleTube)) * sinf(angleTorus);
			m_positions[index * 3 + 2] = m_radiusTube * sinf(angleTube);

			// 法線ベクトル
			m_normals[index * 3 + 0] = cosf(angleTube) * cosf(angleTorus);
			m_normals[index * 3 + 1] = cosf(angleTube) * sinf(angleTorus);
			m_normals[index * 3 + 2] = sinf(angleTube);

			// インデックス
			const size_t indexTorusNext = (indexTorus + 1) % countTorus;
			const size_t indexTubeNext = (indexTube + 1) % countTube;
			m_indices[index * 6 + 0] = static_cast<uint32_t>(indexTorus * countTube + indexTube);
			m_indices[index * 6 + 1] = static_cast<uint32_t>(indexTorusNext * countTube + indexTube);
			m_indices[index * 6 + 2] = static_cast<uint32_t>(indexTorusNext * countTube + indexTubeNext);
			m_indices[index * 6 + 3] = static_cast<uint32_t>(indexTorus * countTube + indexTube);
			m_indices[index * 6 + 4] = static_cast<uint32_t>(indexTorusNext * countTube + indexTubeNext);
			m_indices[index * 6 + 5] = static_cast<uint32_t>(indexTorus * countTube + indexTubeNext);
		}
	}
}

const std::vector<float>& Torus::GetPositions() const
{
	return m_positions;
}

const std::vector<float>& Torus::GetNormals() const
{
	return m_normals;
}

const std::vector<uint32_t>& Torus::GetIndices() const
{
	return m_indices;
}
