﻿#include "Torus.h"

#include <draco/io/file_utils.h>
#include <draco/mesh/mesh.h>
#include <draco/compression/encode.h>
#include <draco/core/encoder_buffer.h>

#include <iostream>
#include <fstream>
#include <memory>

int AddAttribute(
	draco::GeometryAttribute::Type type,
	const std::vector<float>& vector3s,
	draco::Mesh& mesh);
void SetFaces(
	const std::vector<uint32_t>& indices,
	int positionAttributeId,
	int normalAttributeId,
	draco::Mesh& mesh);

int main(void)
{
	Torus torus(2.f, 1.f);
	torus.Generate(10, 10);
	std::cout << "トーラスの頂点とインデックスを生成しました。" << std::endl;

	draco::Mesh mesh;
	const int positionAttributeId = AddAttribute(
		draco::GeometryAttribute::Type::POSITION,
		torus.GetPositions(),
		mesh);
	const int normalAttributeId = AddAttribute(
		draco::GeometryAttribute::Type::NORMAL,
		torus.GetNormals(),
		mesh);
	SetFaces(
		torus.GetIndices(),
		positionAttributeId,
		normalAttributeId,
		mesh);
	std::cout << "Dracoのメッシュに頂点とインデックスを登録しました。" << std::endl;
	std::cout << "頂点数: " << torus.GetPositions().size() / 3 << std::endl;
	std::cout << "インデックス数: " << torus.GetIndices().size() << std::endl;

	draco::Encoder encoder;
	encoder.SetSpeedOptions(0, 0);
	draco::EncoderBuffer buffer;
	const draco::Status status = encoder.EncodeMeshToBuffer(mesh, &buffer);
	if (!status.ok()) 
	{
		std::cout << "Dracoのエンコードに失敗しました。\n"
			<< status.error_msg() << std::endl;
		return -1;
	}
	std::cout << "Dracoの圧縮を行いました。" << std::endl;

	std::ofstream stream("torus.drc", std::ios::out | std::ios::binary);
	stream.write(buffer.data(), buffer.size());
	std::cout << "圧縮結果を torus.drc に書き出しました。" << std::endl;

	std::cout << "\n" <<
		"Position Attribute ID: " << positionAttributeId << "\n" <<
		"Normal Attribute ID: " << normalAttributeId << std::endl;

	return 0;
}

int AddAttribute(
	draco::GeometryAttribute::Type type, 
	const std::vector<float>& vector3s, 
	draco::Mesh& mesh)
{
	using namespace draco;
	assert(vector3s.size() % 3 == 0);
	const uint32_t vertexCount = static_cast<uint32_t>(vector3s.size() / 3);

	GeometryAttribute attribute;
	attribute.Init(type, nullptr, 3, DT_FLOAT32, false, sizeof(float) * 3, 0);
	const int attributeId = mesh.AddAttribute(attribute, true, vertexCount);

	for (uint32_t vIndex = 0; vIndex < vertexCount; ++vIndex)
		mesh.attribute(attributeId)->SetAttributeValue(
			AttributeValueIndex(vIndex),
			vector3s.data() + vIndex * 3);

	return attributeId;
}

void SetFaces(
	const std::vector<uint32_t>& indices,
	int positionAttributeId,
	int normalAttributeId,
	draco::Mesh& mesh)
{
	using namespace draco;
	assert(indices.size() % 3 == 0);

	mesh.attribute(positionAttributeId)->SetExplicitMapping(indices.size());
	mesh.attribute(normalAttributeId)->SetExplicitMapping(indices.size());
	for (uint32_t vIndex = 0; vIndex < indices.size(); ++vIndex)
	{
		const PointIndex pIndex(vIndex);
		const AttributeValueIndex avIndex(indices[vIndex]);
		mesh.attribute(positionAttributeId)->SetPointMapEntry(pIndex, avIndex);
		mesh.attribute(normalAttributeId)->SetPointMapEntry(pIndex, avIndex);
	}

	for (FaceIndex fIndex(0); fIndex < static_cast<uint32_t>(indices.size() / 3); ++fIndex)
	{
		Mesh::Face face;
		for (uint32_t cIndex = 0; cIndex < 3; ++cIndex)
			face[cIndex] = fIndex.value() * 3 + cIndex;
		mesh.SetFace(fIndex, face);
	}
}
